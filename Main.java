package com.company;

public class Main{
    public static void main(String[] args) {
        Integer massiveDefault [] = {31,22,1,4,8,12,16,20,33,55};
        int element;
        for (int i=0; i<=9; i++){
            element = (massiveDefault[i])*110/100;
            massiveDefault[i] = element;
            System.out.println(massiveDefault[i]);
        }
        System.out.println("  ");
        for (int i=0/*целочисленое значение i (порядковый номер элемента нашего массива),это стартовый индекс от которого мы начинаем бежать по элементам нашего массива,в данном случае он равен 0 мы бежим от 1 элемента нашего массива.Важно понимать что порядковый номер(индекс массива) не равен значению элемента этого массива*/; i<massiveDefault.length/*второе условие нашего цикла говорит нам о том до кокого элемента массива побежит наш цикл в данном случае это последний элемент массива так как значение равно Lentgh то есть полной длине массива*/; i++/*Шаг с которым мы будем идти по элементам массива*/){
            for (int j=0; j<massiveDefault.length -1 - i; j++){
                if (massiveDefault[j] < massiveDefault[j + 1]){
                    int temp = massiveDefault[j];
                    massiveDefault[j] = massiveDefault[j+1];
                    massiveDefault[j+1] = temp;
                }
            }
        }
        for (int i=0; i<massiveDefault.length; i++){
            System.out.println(massiveDefault[i]+" ");
        }
    }
}