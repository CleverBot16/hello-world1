package toris;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int sum = 0, razn, ymnoj, num1, num2, ind1 = 0;
        double del;
            System.out.print("Введите два числа: ");
            num1 = in.nextInt();
            num2 = in.nextInt();
            System.out.println("Выберите действие: ");
            System.out.println("1 - Разность");
            System.out.println("2 - Деление");
            System.out.println("3 - Сумма");
            System.out.println("4 - Умножение");
            ind1 = in.nextInt();
            switch (ind1) {
                case 1:
                    razn = num1 - num2;
                    System.out.println("Ответ: " + razn);
                    break;

                case 2:
                    del = ((double) (num1)) / ((double) (num2));
                    System.out.println("Ответ: " + del);
                    break;

                case 3:
                    sum = num1 + num2;
                    System.out.println("Ответ: " + sum);
                    break;

                case 4:
                    ymnoj = num1 * num2;
                    System.out.println("Ответ: " + ymnoj);
                    break;
            }
        }
    }